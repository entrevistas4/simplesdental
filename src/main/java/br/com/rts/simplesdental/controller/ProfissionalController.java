package br.com.rts.simplesdental.controller;

import br.com.rts.simplesdental.controller.request.ProfissionalRequest;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.service.ProfissionalService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@RestController
@RequestMapping("/profissionais")
public class ProfissionalController {

    private final ProfissionalService service;

    @GetMapping
    public ResponseEntity<List<ProfissionalResponse>> buscarTodosProfissionais() {

        List<ProfissionalResponse> profissionalResponses = service.buscarTodosProfissionais();

        if (Objects.isNull(profissionalResponses)
                || profissionalResponses.isEmpty())
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok(profissionalResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProfissionalResponse> buscarProfissional(@PathVariable("id") Long idProfissional) {

        return service.buscarProfissionalResponse(idProfissional)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Long> cadastrarProfissional(@RequestBody ProfissionalRequest profissionalRequest) {

        return ResponseEntity.ok(
                service.cadastrarProfissional(profissionalRequest));
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping("/{id}")
    public void atualizarProfissional(
            @PathVariable("id") Long idProfissional,
            @RequestBody ProfissionalRequest profissionalRequest) {

        service.atualizarProfissional(idProfissional, profissionalRequest);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deletarProfissional(
            @PathVariable("id") Long idProfissional) {

        service.deletarProfissional(idProfissional);
    }
}
