package br.com.rts.simplesdental.controller.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ContatoRequest {

    @NotNull(message = "O campo Nome não pode ser nulo")
    private String nome;

    @NotNull(message = "O campo Contato não pode ser nulo")
    private String contato;

    @NotNull(message = "O campo idProfissional não pode ser nulo")
    private Long idProfissional;
}
