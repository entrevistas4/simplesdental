package br.com.rts.simplesdental.controller.request;

import br.com.rts.simplesdental.enumerator.CargoEnum;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProfissionalRequest {

    @NotNull(message = "O campo Nome não pode ser nulo")
    private String nome;

    @NotNull(message = "O campo Cargo não pode ser nulo")
    private CargoEnum cargo;

    @NotNull(message = "O campo Data nascimento não pode ser nulo")
    private LocalDate dataNascimento;

}
