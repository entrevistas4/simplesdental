package br.com.rts.simplesdental.controller;

import br.com.rts.simplesdental.controller.request.ContatoRequest;
import br.com.rts.simplesdental.controller.response.ContatoResponse;
import br.com.rts.simplesdental.service.ContatoService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@RestController
@RequestMapping("/contatos")
public class ContatosController {

    private final ContatoService service;

    @GetMapping
    public ResponseEntity<List<ContatoResponse>> buscarTodosContatos() {

        List<ContatoResponse> contatoResponses = service.buscarTodosContatos();

        if (Objects.isNull(contatoResponses)
                || contatoResponses.isEmpty())
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok(contatoResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContatoResponse> buscarContato(@PathVariable("id") Long idContato) {

        return service.buscarContato(idContato)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Long> cadastrarContato(@RequestBody ContatoRequest contatoRequest) {

        return ResponseEntity.ok(
                service.cadastrarContato(contatoRequest));
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping("/{id}")
    public void atualizarContato(
            @PathVariable("id") Long idContato,
            @RequestBody ContatoRequest contatoRequest) {

        service.atualizarContato(idContato, contatoRequest);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deletarContato(
            @PathVariable("id") Long idContato) {

        service.deletarContato(idContato);
    }
}
