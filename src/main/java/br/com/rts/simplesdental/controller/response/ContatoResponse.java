package br.com.rts.simplesdental.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
public class ContatoResponse {

    private Long id;

    private String nome;

    private String contato;

    private ProfissionalResponse profissional;

}
