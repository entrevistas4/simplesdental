package br.com.rts.simplesdental.controller.response;

import br.com.rts.simplesdental.enumerator.CargoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
public class ProfissionalResponse {

    private Long id;

    private String nome;

    private CargoEnum cargo;

    private LocalDate dataNascimento;
}
