package br.com.rts.simplesdental.mapper;

import br.com.rts.simplesdental.controller.request.ProfissionalRequest;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.entity.Profissional;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.Optional;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProfissionalMapper {
    Profissional requestToEntity(ProfissionalRequest profissionalRequest);

    ProfissionalResponse entityToResponse(Profissional profissional);
}
