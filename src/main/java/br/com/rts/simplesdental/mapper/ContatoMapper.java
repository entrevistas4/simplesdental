package br.com.rts.simplesdental.mapper;


import br.com.rts.simplesdental.controller.request.ContatoRequest;
import br.com.rts.simplesdental.controller.response.ContatoResponse;
import br.com.rts.simplesdental.entity.Contato;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = ProfissionalMapper.class)
public interface ContatoMapper {

    ContatoResponse entityToResponse(Contato contato);

    Contato requestToEntity(ContatoRequest contatoRequest);
}
