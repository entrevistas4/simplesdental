package br.com.rts.simplesdental.exception;

import br.com.rts.simplesdental.controller.response.ExceptionResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ProfissionalException.class, ContatoException.class})
    private ResponseEntity<?> exceptionHandlerInternal(RuntimeException exception, WebRequest request) {

        ExceptionResponse exceptionResponse =
                ExceptionResponse
                        .builder()
                        .errorDateTime(LocalDateTime.now())
                        .errorMessage(exception.getMessage())
                        .status(HttpStatus.BAD_REQUEST)
                        .build();

        return handleExceptionInternal(exception, exceptionResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
