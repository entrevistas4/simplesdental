package br.com.rts.simplesdental.exception;

public class ProfissionalException extends RuntimeException {

    public ProfissionalException(String message) {
        super(message);
    }
}
