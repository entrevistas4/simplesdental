package br.com.rts.simplesdental.exception;

public class ContatoException extends RuntimeException {

    public ContatoException(String message) {
        super(message);
    }
}
