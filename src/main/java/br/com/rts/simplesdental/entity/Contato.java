package br.com.rts.simplesdental.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.Hibernate;

import java.time.LocalDateTime;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Getter
@Setter
@ToString
@Entity
@Table(name = "TB_CONTATO")
public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(length = 150)
    @Size(message = "O nome deve ter entre 5 e 150 caracteres", min = 5, max = 150)
    private String nome;

    private String contato;

    private LocalDateTime createdDate;

    @NotNull(message = "O profissional é obrigatório")
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "profissional_id")
    private Profissional profissional;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Contato contato = (Contato) o;
        return getId() != null && Objects.equals(getId(), contato.getId());
    }

}
