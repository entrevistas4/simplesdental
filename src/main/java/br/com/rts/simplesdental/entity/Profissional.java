package br.com.rts.simplesdental.entity;

import br.com.rts.simplesdental.enumerator.CargoEnum;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder(toBuilder = true)
@Entity
@Table(name = "TB_PROFISSIONAL")
public class Profissional {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(length = 150)
    @Size(message = "O Nome deve ter entre 5 e 150 caracteres", min = 5, max = 150)
    private String nome;

    @NotNull(message = "O Cargo é obrigatório")
    @Enumerated(EnumType.STRING)
    @Column(name = "cargo")
    private CargoEnum cargo;

    @FutureOrPresent(message = "A data informada não é valida")
    @Future(message = "A data informada não é valida")
    @NotNull(message = "Data de Nascimento é obrigatório")
    private LocalDate dataNascimento;

    @Column(updatable = false)
    private LocalDateTime createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Profissional that = (Profissional) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

}
