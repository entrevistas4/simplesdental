package br.com.rts.simplesdental.service;

import br.com.rts.simplesdental.controller.request.ProfissionalRequest;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.entity.Profissional;

import java.util.List;
import java.util.Optional;

public interface ProfissionalService {

    Profissional buscarProfissional(Long idProfissional);

    List<ProfissionalResponse> buscarTodosProfissionais();

    Optional<ProfissionalResponse> buscarProfissionalResponse(Long idProfissional);

    Long cadastrarProfissional(ProfissionalRequest profissionalRequest);

    void atualizarProfissional(Long idProfissional, ProfissionalRequest profissionalRequest);

    void deletarProfissional(Long idProfissional);
}
