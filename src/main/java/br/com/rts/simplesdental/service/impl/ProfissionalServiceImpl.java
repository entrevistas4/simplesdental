package br.com.rts.simplesdental.service.impl;

import br.com.rts.simplesdental.controller.request.ProfissionalRequest;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.entity.Profissional;
import br.com.rts.simplesdental.exception.ProfissionalException;
import br.com.rts.simplesdental.mapper.ProfissionalMapper;
import br.com.rts.simplesdental.repository.ProfissionalRepository;
import br.com.rts.simplesdental.service.ProfissionalService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@Service
public class ProfissionalServiceImpl implements ProfissionalService {

    private ProfissionalRepository repository;

    private ProfissionalMapper mapper;


    @Override
    public Profissional buscarProfissional(Long idProfissional) {
        log.info("buscarProfissional({})", idProfissional);

        return repository.findById(idProfissional).orElseThrow(() -> new ProfissionalException("Profissional não existe"));
    }

    @Override
    public List<ProfissionalResponse> buscarTodosProfissionais() {

        return repository.findAll()
                .stream()
                .map(mapper::entityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ProfissionalResponse> buscarProfissionalResponse(Long idProfissional) {
        return repository.findById(idProfissional)
                .map(mapper::entityToResponse);
    }

    @Override
    public Long cadastrarProfissional(ProfissionalRequest profissionalRequest) {

        Profissional profissional = mapper.requestToEntity(profissionalRequest);
        profissional.setCreatedDate(LocalDateTime.now());

        return repository.save(profissional).getId();
    }

    @Override
    public void atualizarProfissional(Long idProfissional, ProfissionalRequest profissionalRequest) {

        Profissional profissional = repository.findById(idProfissional)
                .orElseThrow(() -> new ProfissionalException("Este profissional não existe!"));

        repository.save(
                profissional.toBuilder()
                        .nome(profissionalRequest.getNome())
                        .cargo(profissionalRequest.getCargo())
                        .dataNascimento(profissionalRequest.getDataNascimento())
                        .build());
    }

    @Override
    public void deletarProfissional(Long idProfissional) {

        if (!repository.existsById(idProfissional))
            throw new ProfissionalException("Este contato não existe!");

        repository.deleteById(idProfissional);
    }

}
