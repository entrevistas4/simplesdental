package br.com.rts.simplesdental.service;

import br.com.rts.simplesdental.controller.request.ContatoRequest;
import br.com.rts.simplesdental.controller.response.ContatoResponse;

import java.util.List;
import java.util.Optional;

public interface ContatoService {

    List<ContatoResponse> buscarTodosContatos();

    Optional<ContatoResponse> buscarContato(Long idContato);

    Long cadastrarContato(ContatoRequest contatoRequest);

    void atualizarContato(Long idContato, ContatoRequest contatoRequest);

    void deletarContato(Long idContato);
}
