package br.com.rts.simplesdental.service.impl;

import br.com.rts.simplesdental.controller.request.ContatoRequest;
import br.com.rts.simplesdental.controller.response.ContatoResponse;
import br.com.rts.simplesdental.entity.Contato;
import br.com.rts.simplesdental.exception.ContatoException;
import br.com.rts.simplesdental.mapper.ContatoMapper;
import br.com.rts.simplesdental.repository.ContatoRepository;
import br.com.rts.simplesdental.service.ContatoService;
import br.com.rts.simplesdental.service.ProfissionalService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
@AllArgsConstructor
public class ContatoServiceImpl implements ContatoService {

    private ContatoRepository repository;

    private ContatoMapper mapper;

    private ProfissionalService profissionalService;

    @Override
    public List<ContatoResponse> buscarTodosContatos() {

        return repository.findAll()
                .stream()
                .map(mapper::entityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ContatoResponse> buscarContato(Long idContato) {
        return repository.findById(idContato)
                .map(mapper::entityToResponse);
    }

    @Override
    public Long cadastrarContato(ContatoRequest contatoRequest) {

        Contato contato = mapper.requestToEntity(contatoRequest);
        contato.setCreatedDate(LocalDateTime.now());
        contato.setProfissional(
                profissionalService.buscarProfissional(contatoRequest.getIdProfissional()));

        return repository.save(contato).getId();
    }

    @Override
    public void atualizarContato(Long idContato, ContatoRequest contatoRequest) {

        Contato contato = repository.findById(idContato)
                .orElseThrow(() -> new ContatoException("Este contato não existe!"));

        repository.save(
                contato.toBuilder()
                        .nome(contatoRequest.getNome())
                        .contato(contatoRequest.getContato())
                        .profissional(
                                profissionalService.buscarProfissional(contatoRequest.getIdProfissional()))
                        .build());
    }

    @Override
    public void deletarContato(Long idContato) {

        if (!repository.existsById(idContato))
            throw new ContatoException("Este contato não existe!");

        repository.deleteById(idContato);
    }
}
