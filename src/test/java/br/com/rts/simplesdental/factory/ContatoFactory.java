package br.com.rts.simplesdental.factory;

import br.com.rts.simplesdental.entity.Contato;
import br.com.rts.simplesdental.entity.Profissional;

import java.time.LocalDateTime;

public class ContatoFactory {

    public static Contato getBuild(){

        return Contato
                .builder()
                .id(1L)
                .nome("Teste 123")
                .contato("556798111111")
                .profissional(ProfissionalFactory.getBuild())
                .createdDate(LocalDateTime.now())
                .build();
    }
}
