package br.com.rts.simplesdental.factory;

import br.com.rts.simplesdental.entity.Profissional;
import br.com.rts.simplesdental.enumerator.CargoEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ProfissionalFactory {

    public static Profissional getBuild(){

        return Profissional
                .builder()
                .id(1L)
                .dataNascimento(LocalDate.of(1986, 9, 25))
                .createdDate(LocalDateTime.now())
                .nome("Profissional Teste")
                .cargo(CargoEnum.DESENVOLVEDOR)
                .build();
    }
}
