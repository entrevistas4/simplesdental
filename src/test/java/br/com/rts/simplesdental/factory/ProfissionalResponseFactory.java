package br.com.rts.simplesdental.factory;

import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.enumerator.CargoEnum;

import java.time.LocalDate;

public class ProfissionalResponseFactory {
    public static ProfissionalResponse getBuild() {

        return ProfissionalResponse
                .builder()
                .nome("Teste Profissional 1")
                .id(1L)
                .cargo(CargoEnum.DESENVOLVEDOR)
                .dataNascimento(LocalDate.of(1986, 9, 25))
                .build();
    }
}
