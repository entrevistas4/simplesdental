package br.com.rts.simplesdental.factory;

import br.com.rts.simplesdental.controller.response.ContatoResponse;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;

public class ContatoResponseFactory {
    public static ContatoResponse getBuild() {
        return ContatoResponse
                .builder()
                .id(1L)
                .contato("55979811111")
                .profissional(ProfissionalResponseFactory.getBuild())
                .nome("Teste Contato 123")
                .build();

    }
}
