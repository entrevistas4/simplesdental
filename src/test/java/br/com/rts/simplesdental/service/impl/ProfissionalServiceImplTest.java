package br.com.rts.simplesdental.service.impl;

import br.com.rts.simplesdental.controller.request.ProfissionalRequest;
import br.com.rts.simplesdental.controller.response.ProfissionalResponse;
import br.com.rts.simplesdental.enumerator.CargoEnum;
import br.com.rts.simplesdental.factory.ProfissionalFactory;
import br.com.rts.simplesdental.factory.ProfissionalResponseFactory;
import br.com.rts.simplesdental.mapper.ProfissionalMapper;
import br.com.rts.simplesdental.repository.ProfissionalRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProfissionalServiceImplTest {

    @InjectMocks
    ProfissionalServiceImpl profissionalService;

    @Mock
    ProfissionalRepository repository;

    @Mock
    ProfissionalMapper mapper;

    @Test
    void testBuscarTodosContatos() {

        when(repository.findAll()).thenReturn(Collections.singletonList(ProfissionalFactory.getBuild()));

        List<ProfissionalResponse> result = profissionalService.buscarTodosProfissionais();

        Assertions.assertEquals(1, result.size());
    }

    @Test
    void testBuscarTodosContatosWithoutResults() {
        when(repository.findAll()).thenReturn(new ArrayList<>());

        List<ProfissionalResponse> result = profissionalService.buscarTodosProfissionais();

        Assertions.assertEquals(0, result.size());
    }

    @Test
    void testBuscarContato() {

        when(repository.findById(1L)).thenReturn(Optional.of(ProfissionalFactory.getBuild()));

        when(mapper.entityToResponse(ProfissionalFactory.getBuild())).thenReturn(ProfissionalResponseFactory.getBuild());

        Optional<ProfissionalResponse> result = profissionalService.buscarProfissionalResponse(1L);

        Assertions.assertTrue(result.isPresent());
    }

    @Test
    void testBuscarContatoSemResultado() {
        when(repository.findById(any())).thenReturn(Optional.empty());

        Optional<ProfissionalResponse> result = profissionalService.buscarProfissionalResponse(1L);

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testCadastrarContato() {
        when(mapper.requestToEntity(any())).thenReturn(ProfissionalFactory.getBuild());

        when(repository.save(any())).thenReturn(ProfissionalFactory.getBuild());

        Long result = profissionalService.cadastrarProfissional(new ProfissionalRequest("nome", CargoEnum.DESENVOLVEDOR, LocalDate.now()));

        Assertions.assertEquals(Long.valueOf(1), result);
    }

    @Test
    void testAtualizarContato() {

        when(repository.findById(1L)).thenReturn(Optional.of(ProfissionalFactory.getBuild()));

        profissionalService.atualizarProfissional(1L, new ProfissionalRequest("nome", CargoEnum.DESENVOLVEDOR, LocalDate.now()));
    }

    @Test
    void testDeletarContato() {

        when(repository.existsById(1L)).thenReturn(true);

        profissionalService.deletarProfissional(1L);
    }
}