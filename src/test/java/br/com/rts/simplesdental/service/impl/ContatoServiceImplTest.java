package br.com.rts.simplesdental.service.impl;

import br.com.rts.simplesdental.controller.request.ContatoRequest;
import br.com.rts.simplesdental.controller.response.ContatoResponse;
import br.com.rts.simplesdental.factory.ContatoFactory;
import br.com.rts.simplesdental.factory.ContatoResponseFactory;
import br.com.rts.simplesdental.factory.ProfissionalFactory;
import br.com.rts.simplesdental.mapper.ContatoMapper;
import br.com.rts.simplesdental.repository.ContatoRepository;
import br.com.rts.simplesdental.service.ProfissionalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ContatoServiceImplTest {

    @InjectMocks
    ContatoServiceImpl contatoServiceImpl;

    @Mock
    ContatoRepository repository;

    @Mock
    ContatoMapper mapper;

    @Mock
    ProfissionalService profissionalService;

    @Test
    void testBuscarTodosContatos() {
        when(repository.findAll()).thenReturn(Collections.singletonList(ContatoFactory.getBuild()));

        List<ContatoResponse> result = contatoServiceImpl.buscarTodosContatos();

        Assertions.assertEquals(1, result.size());
    }

    @Test
    void testBuscarTodosContatosWithoutResults() {
        when(repository.findAll()).thenReturn(new ArrayList<>());

        List<ContatoResponse> result = contatoServiceImpl.buscarTodosContatos();

        Assertions.assertEquals(0, result.size());
    }

    @Test
    void testBuscarContato() {
        when(repository.findById(1L)).thenReturn(Optional.of(ContatoFactory.getBuild()));

        when(mapper.entityToResponse(ContatoFactory.getBuild())).thenReturn(ContatoResponseFactory.getBuild());

        Optional<ContatoResponse> result = contatoServiceImpl.buscarContato(1L);

        Assertions.assertTrue(result.isPresent());
    }

    @Test
    void testBuscarContatoSemResultado() {
        when(repository.findById(any())).thenReturn(Optional.empty());

        Optional<ContatoResponse> result = contatoServiceImpl.buscarContato(1L);

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testCadastrarContato() {
        when(mapper.requestToEntity(any())).thenReturn(ContatoFactory.getBuild());

        when(profissionalService.buscarProfissional(anyLong())).thenReturn(ProfissionalFactory.getBuild());

        when(repository.save(any())).thenReturn(ContatoFactory.getBuild());

        Long result = contatoServiceImpl.cadastrarContato(new ContatoRequest("nome", "contato", 1L));

        Assertions.assertEquals(Long.valueOf(1), result);
    }

    @Test
    void testAtualizarContato() {
        when(profissionalService.buscarProfissional(anyLong())).thenReturn(ProfissionalFactory.getBuild());

        when(repository.findById(1L)).thenReturn(Optional.of(ContatoFactory.getBuild()));

        contatoServiceImpl.atualizarContato(1L, new ContatoRequest("nome", "contato", 1L));
    }

    @Test
    void testDeletarContato() {

        when(repository.existsById(1L)).thenReturn(true);

        contatoServiceImpl.deletarContato(1L);
    }
}